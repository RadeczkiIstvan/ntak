unit obj.Ntak.Client;

interface

uses
  obj.Ntak.HttpClient,
  obj.Ntak.JWS;

type
  TNtakClient = class
  private
    fNtakHttpClient: TNtakHttpClient;
    fNtakJWS: TNtakJWS;
    fCertificate: string;
  public
    constructor Create(aHttpClient: TNtakHttpClient; aJWS: TNtakJWS; const aCertificate: string);
    destructor Destroy; override;

    procedure SoldTicket(const aData: string);

    property NtakHttpClient: TNtakHttpClient read fNtakHttpClient;
  end;

implementation

uses
  JOSE.Core.Base;

{ TNtakClient }

constructor TNtakClient.Create(aHttpClient: TNtakHttpClient; aJWS: TNtakJWS; const aCertificate: string);
begin
  fNtakHttpClient := aHttpClient;
  fNtakJWS := aJWS;
  fCertificate := aCertificate;
end;

destructor TNtakClient.Destroy;
begin
  fNtakHttpClient := nil;
  fNtakJWS := nil;
  inherited;
end;

procedure TNtakClient.SoldTicket(const aData: string);
var
  sign: string;
  data : string;
begin
  fNtakJWS.CalcSign(aData);

  sign := fNtakJWS.Header + '..' + fNtakJWS.Signature;
  data:=ToJSON(fNtakJWS.JWT.Claims.JSON); //musz�j mert a sign is �gy haszn�lja
  fNtakHttpClient.SendData('/api/jegyertekesites', data, sign, fCertificate);
end;

end.

