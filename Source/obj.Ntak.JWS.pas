unit obj.Ntak.JWS;

interface

uses
  JOSE.Signing.RSA,
  JOSE.Types.Bytes,
  IdSSLOpenSSLHeaders,
  JOSE.OpenSSL.Headers,
  JOSE.Core.JWT,
  JOSE.Core.JWS,
  JOSE.Core.JWE,
  JOSE.Core.JWK,
  JOSE.Core.JWA,
  JOSE.Types.JSON,
  JOSE.Encoding.Base64;

type
  TNtakJWS = class
  private
    fLKeyPair: TKeyPair;
    fJWT: TJWT;
    FHeader: string;
    FSignature: string;
    FPayload: string;
    procedure SetHeader(const Value: string);
    procedure SetPayload(const Value: string);
    procedure SetSignature(const Value: string);
  public
    constructor Create(const aPrimaryKey: string);
    destructor Destroy; override;

    procedure CalcSign(const aData: string);

    property JWT: TJWT read fJWT;
    property Header: string read FHeader write SetHeader;
    property Payload: string read FPayload write SetPayload;
    property Signature: string read FSignature write SetSignature;
  end;

implementation

{ TNtakJWS }

procedure TNtakJWS.CalcSign(const aData: string);
var
  LSigner: TJWS;
  JOSEAlg: TJOSEAlgorithmId;
begin
  JOSEAlg := TJOSEAlgorithmId.RS256;
  Header := '';
  Payload := '';
  Signature := '';
  LSigner := TJWS.Create(fJWT);
  try
    fJWT.Claims.SetNewJSON(aData);
    LSigner.SkipKeyValidation := True;
    LSigner.Sign(fLKeyPair.PrivateKey, JOSEAlg);
    Header:=LSigner.Header;
    Signature:=LSigner.Signature;
    Payload:=LSigner.Payload;
  finally
    LSigner.Free;
  end;
end;

constructor TNtakJWS.Create(const aPrimaryKey: string);
begin
  fJWT := TJWT.Create;
  fJWT.Header.JSON := TJSONObject(TJSONObject.ParseJSONValue('{"alg": "RS256"}'));
  fLKeyPair := TKeyPair.Create;
  fLKeyPair.PrivateKey.Key := aPrimaryKey;
end;

destructor TNtakJWS.Destroy;
begin
  fLKeyPair.Free;
  fJWT.Free;
  inherited;
end;

procedure TNtakJWS.SetHeader(const Value: string);
begin
  FHeader := Value;
end;

procedure TNtakJWS.SetPayload(const Value: string);
begin
  FPayload := Value;
end;

procedure TNtakJWS.SetSignature(const Value: string);
begin
  FSignature := Value;
end;

end.

