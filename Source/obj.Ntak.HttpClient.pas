unit obj.Ntak.HttpClient;

interface

uses
  System.Classes,
  IdBaseComponent,
  IdComponent,
  IdTCPConnection,
  IdTCPClient,
  IdHTTP,
  IdServerIOHandler,
  IdSSL,
  IdSSLOpenSSL;

type
  TNtakHttpClient = class
  private
    fRequestUrl: string;
    fRequestTxt: string;
    fResponseTxt: string;
    fBasePath: string;
    fHttp: TIdHTTP;
    fSSL: TIdSSLIOHandlerSocketOpenSSL;
    fIsSuccess: Boolean;
    fUseExternalClient: Boolean;
  public
    constructor Create(const aBasePath, aCertFile, aKeyFile: string; aHttp: TIdHTTP);
    destructor Destroy; override;

    procedure SendData(const aPath, aData, aJwsSignature, aCertificate: string);

    property RequestUrl: string read fRequestUrl;
    property RequestTxt: string read fRequestTxt;
    property ResponseTxt: string read fResponseTxt;
    property IsSuccess: Boolean read fIsSuccess;
    property Http: TIdHTTP read fHttp;
  end;

implementation

uses
  System.SysUtils,
  IdException,
  IdStack;

{ TNtakClient }

constructor TNtakHttpClient.Create(const aBasePath, aCertFile, aKeyFile: string; aHttp: TIdHTTP);
begin
  fUseExternalClient := Assigned(aHttp);
  fBasePath := aBasePath;
  fRequestUrl := '';
  fRequestTxt := '';
  fResponseTxt := '';

  if fUseExternalClient then
  begin
    Exit;
  end;

  fHttp := TIdHTTP.Create(nil);
  fSSL := TIdSSLIOHandlerSocketOpenSSL.Create(nil);
  fSSL.SSLOptions.Method:=sslvTLSv1_2;
  fSSL.SSLOptions.SSLVersions:=[sslvTLSv1_2];
  fSSL.SSLOptions.CertFile:=aCertFile;
  fSSL.SSLOptions.KeyFile:=aKeyFile;
  fHttp.IOHandler := fSSL;
  fHttp.Request.CharSet := 'utf-8';
  fHttp.Request.ContentType := 'application/json';
  fHttp.Request.Accept := 'application/json';
  fHttp.Request.AcceptEncoding := 'utf-8';
end;

destructor TNtakHttpClient.Destroy;
begin
  if fUseExternalClient then
  begin
    fHttp.IOHandler := nil;
    fSSL.Free;
    fHttp.Free;
  end;

  inherited;
end;

procedure TNtakHttpClient.SendData(const aPath, aData, aJwsSignature, aCertificate: string);
var
  reqStrm, resStrm: TStringStream;
begin
  fRequestTxt := aData;
  reqStrm := TStringStream.Create(aData, TEncoding.UTF8);
  try
    resStrm := TStringStream.Create('', TEncoding.UTF8);
    try
      fHttp.Request.CustomHeaders.Values['x-jws-signature'] := aJwsSignature;
      fHttp.Request.CustomHeaders.Values['x-certificate'] := aCertificate;

      fRequestUrl := fBasePath + aPath;
      fIsSuccess := False;
      try
        fHttp.Post(fRequestUrl, reqStrm, resStrm);
        fIsSuccess := True;
        fResponseTxt := resStrm.DataString;
      except
        on e: EIdHTTPProtocolException do
        begin
          fResponseTxt := Format('%s: %s', [e.ClassName, e.ErrorMessage]);
        end;
        on e: EIdConnClosedGracefully do
        begin
          fResponseTxt := Format('%s: %s', [e.ClassName, e.Message]);
        end;
        on e: EIdSocketError do
        begin
          fResponseTxt := Format('%s: %s', [e.ClassName, e.Message]);
        end;
        on e: EIdException do
        begin
          fResponseTxt := Format('%s: %s', [e.ClassName, e.Message]);
        end;
        on e: Exception do
        begin
          fResponseTxt := Format('%s: %s', [e.ClassName, e.Message]);
        end;
      end;
    finally
      resStrm.Free;
    end;
  finally
    reqStrm.Free;
  end;
end;

end.

