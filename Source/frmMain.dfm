object formMain: TformMain
  Left = 0
  Top = 0
  Caption = 'NTAK comm test'
  ClientHeight = 299
  ClientWidth = 852
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object mmoLog: TMemo
    Left = 0
    Top = 89
    Width = 852
    Height = 210
    Align = alClient
    ScrollBars = ssBoth
    TabOrder = 1
  end
  object pnl1: TPanel
    Left = 0
    Top = 0
    Width = 852
    Height = 89
    Align = alTop
    Caption = 'pnl1'
    TabOrder = 0
    object btnTest: TButton
      Left = 335
      Top = 3
      Width = 242
      Height = 74
      Caption = 'Teszt'
      TabOrder = 0
      OnClick = btnTestClick
    end
    object edtCertFile: TEdit
      Left = 16
      Top = 5
      Width = 313
      Height = 21
      TabOrder = 1
      Text = 'd:\Works\ExternalDocs\NTAK\22001359\22001359_cert.cer'
    end
    object edtKeyFile: TEdit
      Left = 16
      Top = 32
      Width = 313
      Height = 21
      TabOrder = 2
      Text = 'd:\Works\ExternalDocs\NTAK\22001359\22001359_key.key'
    end
    object edtUrl: TEdit
      Left = 16
      Top = 56
      Width = 145
      Height = 21
      TabOrder = 4
      Text = 'https://tss.tesztntak.hu'
    end
    object edtJSONFile: TEdit
      Left = 167
      Top = 56
      Width = 162
      Height = 21
      TabOrder = 5
      Text = 'd:\ntak001.json'
    end
    object chkIndyLog: TCheckBox
      Left = 608
      Top = 40
      Width = 97
      Height = 17
      Caption = 'Indy log'
      TabOrder = 3
    end
  end
  object idlgvnt1: TIdLogEvent
    OnReceive = idlgvnt1Receive
    OnSend = idlgvnt1Send
    Active = True
    Left = 792
    Top = 16
  end
end
