unit Ntak.Tools;

interface

function LoadPemFileToString(const aFileName: string): string;

implementation

uses
  System.Classes,
  System.SysUtils;

function LoadPemFileToString(const aFileName: string): string;
var
  strl: TStringList;
begin
  Result := '';
  if not FileExists(aFileName) then
  begin
    raise EFileNotFoundException.CreateFmt('File not found! %s', [aFileName]);
  end;

  strl := TStringList.Create;
  try
    strl.LoadFromFile(aFileName);
    strl.Delete(0);
    strl.Delete(strl.Count - 1);
    strl.LineBreak:='';
    Result := strl.Text;
  finally
    strl.Free;
  end;

end;

end.

