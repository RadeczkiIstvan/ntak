unit frmMain;

interface

uses
  Winapi.Windows,
  Winapi.Messages,
  System.SysUtils,
  System.Variants,
  System.Classes,
  Vcl.Graphics,
  Vcl.Controls,
  Vcl.Forms,
  Vcl.Dialogs,
  Vcl.StdCtrls,
  IdIntercept,
  IdGlobal,
  IdBaseComponent,
  IdLogBase,
  IdLogEvent,
  Vcl.ExtCtrls;

type
  TformMain = class(TForm)
    mmoLog: TMemo;
    btnTest: TButton;
    idlgvnt1: TIdLogEvent;
    pnl1: TPanel;
    edtCertFile: TEdit;
    edtKeyFile: TEdit;
    edtUrl: TEdit;
    edtJSONFile: TEdit;
    chkIndyLog: TCheckBox;
    procedure btnTestClick(Sender: TObject);
    procedure idlgvnt1Send(ASender: TIdConnectionIntercept; var ABuffer: TIdBytes);
    procedure idlgvnt1Receive(ASender: TIdConnectionIntercept; var ABuffer: TIdBytes);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  formMain: TformMain;

implementation

uses
  JOSE.Types.Bytes,
  obj.Ntak.HttpClient,
  obj.Ntak.JWS,
  obj.Ntak.Client,
  Ntak.Tools;

{$R *.dfm}

procedure TformMain.btnTestClick(Sender: TObject);
var
  sdata, skey: TStringStream;
  ntakClient: TNtakClient;
  httpClient: TNtakHttpClient;
  ntakJWS: TNtakJWS;
  cert: TJOSEBytes;
begin
  cert := LoadPemFileToString(edtCertFile.Text);
  sdata := TStringStream.Create;
  try
    sdata.LoadFromFile(edtJSONFile.Text);
    skey := TStringStream.Create;
    try
      skey.LoadFromFile(edtKeyFile.Text);
      ntakJWS := TNtakJWS.Create(skey.DataString);
      try
        httpClient := TNtakHttpClient.Create(edtUrl.Text, edtCertFile.Text, edtKeyFile.Text, nil);
        try
          if chkIndyLog.Checked then
          begin
            httpClient.Http.Intercept := idlgvnt1;
          end
          else
          begin
            httpClient.Http.Intercept := nil;
          end;
          ntakClient := TNtakClient.Create(httpClient, ntakJWS, cert);
          try
            ntakClient.SoldTicket(sdata.DataString);

          finally
            mmoLog.Lines.Add(StringOfChar('*', 80));
            mmoLog.Lines.Add(httpClient.RequestUrl);
            mmoLog.Lines.Add(StringOfChar('-', 80));
            mmoLog.Lines.Add(httpClient.RequestTxt);
            mmoLog.Lines.Add(StringOfChar('-', 80));
            mmoLog.Lines.Add(httpClient.ResponseTxt);
            mmoLog.Lines.Add(StringOfChar('*', 80));
            ntakClient.Free;
          end;
        finally
          httpClient.Free;
        end;
      finally
        ntakJWS.Free;
      end;
    finally
      skey.Free;
    end;
  finally
    sdata.Free;
  end;
end;

procedure TformMain.idlgvnt1Receive(ASender: TIdConnectionIntercept; var ABuffer: TIdBytes);
begin
  mmoLog.Lines.Add(StringOfChar('#', 80));
  mmoLog.Lines.Add(BytesToString(ABuffer));
  mmoLog.Lines.Add(StringOfChar('#', 80));
end;

procedure TformMain.idlgvnt1Send(ASender: TIdConnectionIntercept; var ABuffer: TIdBytes);
begin
  mmoLog.Lines.Add(StringOfChar('#', 80));
  mmoLog.Lines.Add(BytesToString(ABuffer));
  mmoLog.Lines.Add(StringOfChar('#', 80));
end;

end.

